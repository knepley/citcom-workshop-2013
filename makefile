PDFLATEX = pdflatex
BIBTEX   = bibtex

Report.pdf: Report.tex preamble.tex paper.tex paper.bib
	${PDFLATEX} Report.tex
	BIBINPUTS=${BIBINPUTS}:${PETSC_DIR}/src/docs/tex:${HOME}/Documents ${BIBTEX} Report
	${PDFLATEX} Report.tex
	${PDFLATEX} Report.tex
