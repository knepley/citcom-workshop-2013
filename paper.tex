\section{{\large Overview}}
Members of the \href{http://www.geodynamics.org/cig/community/workinggroups/cs}{Computational Science Working Group} recently organized a small workshop on implementing solvers in CitcomCU and CitcomS. The goal was to have members of the CIG Computational Science Working Group meet with CIG staff and several members of the mantle convection community to address specific avenues for furthering the solver capabilities of several CIG community codes, with a focus on CitcomCU and CitcomS in particular. The workshop was a combined onsite and virtual meeting hosted at the CIG facilities on the UC Davis campus, September 16-17, 2013. Approximately 16 participants attended, with 11 people onsite and 5 connecting through Adobe Connect. A group size of less than 20 was targeted, as the goal was to have a hands on meeting with ample time spent in the code and focused code implementation. The list of participants and workshop agenda is included in Section \ref{sec:info}. \\
\\
This meeting was motivated by the need for more robust solvers in mantle convection codes, so that the computational software keeps pace with the increasing complexity in the science and the advances in hardware and computational facilities. For example, previous work has shown that large viscosity variations occurring over short distances pose a challenge for computational codes, and models with complex 3D geometries require substantially greater numbers of elements, increasing the computational demands \citep{Moresi_Solomatov_1995,Moresi_et_al_1996,Tackley_1996,May_Moresi_08,Geenan_et_al_09,BursteddeGhattasStadlerTuWilcox09,Jadamec_Billen_2010,Furuichi_et_al_11,Jadamec_etal_xsede2012}.  In addition, for complex time-dependent problems, the computational runs often face temperature overshoots. Thus, there are immediate computational challenges facing the mantle convection community in terms of the capabilities of the stokes solvers and solver for the energy equation. \\
\\
Therefore, this workshop was convened to gather both computational scientists and several members of the mantle convection community to work out the details of the specific code modifications that would be required. This work session follows from goals identified in the workplan, ``Prioritized List of Proposed Work Related to CitcomS/CitcomCU'', that was drafted in September 2012, and compiled by the \href{http://www.geodynamics.org/cig/community/workinggroups/mc}{Mantle Convection Working Group}. A copy of the workplan is attached as Appendix A.

%%%
%%%
%%%
\section{{\large Outcomes}}
The workshop comprised of two days of short presentations, with the presentations followed by lengthy discussion, and examination of the CitcomCU and CitcomS source code. The three primary categories covered were the Stokes Solvers, the energy equation solver, and mantle convection/subduction benchmarks. The implementation of the energy equation solver in the CIG supported code Aspect was also discussed at length.  The workshop outcomes are described in the sections that follow.  See Section \ref{sec:info} for the meeting agenda.

\subsection{Stokes Flow}
Margarete Jadamec presented an overview of the scientific drivers and motivation for increasing the solver functionality in CitcomCU and CitcomS. She showed results from her recent study optimizing the full multigrid parameters in CitcomCU for 3D subduction models with a non-Newtonian viscosity, and hence large viscosity variations \citep{Jadamec_etal_xsede2012}. Improvements on runtime by up to 30\% were achieved, which is significant for models run on hundreds to thousands of cores \citep{Jadamec_etal_xsede2012}. In addition, the initial code performance and sensitivity results for Citcom from \cite{Moresi_Solomatov_1995} and \cite{Moresi_et_al_1996} for models with large viscosity variations were summarized.  Shijie Zhong presented a review of the Citcom, CitcomCU, and CitcomS architecture and a description of how the Stokes flow problem is solved including the pressure equation and the conjugate gradient method and the Uzawa algorithm which uses the geometric multigrid on the inner solve \citep{Moresi_Solomatov_1995,Moresi_et_al_1996,Zhong_et_al_00,Zhong_2006,Zhong_etal_08}. Recommendations for improvements were suggested including using a non-linear solver for better convergence, improving the Stokes solver for models using weak zones, and improving the energy equation to reduce the overshoot problems.

\subsubsection{Nonlinear Solvers}

Matt Knepley discussed the inclusion of a nonlinear solver in Citcom. Nonlinear solvers, such as Newton's method,
perhaps preconditioned with the nonlinear Generalized Minimum Residual Method (NGMRES), could greatly improve the convergence for difficult viscosity
distributions, such as those seen in subduction models~\citep{Jadamec_Billen_2010}. A trial implementation is available on 
\href{https://bitbucket.org/knepley/citcomp}{Bitbucket} which used PETSc. A model of the data distribution in Citcom was
made using the PETSc DMDA class, which handles Cartesian grids, so that the communication pattern could be replicated. Then a routine was added to
calculate the full residual, for velocity and pressure. This residual routine could be used by the PETSc SNES solver for
nonlinear equations. In future work, an analytic Jacobian routine could also be added, although PETSc currently uses
graph coloring to construct one automatically. There appeared to be a small discrepancy between the boundary values on
different processes during the Citcom linear solve, and during the nonlinear solver update, which is being
investigated.

\subsubsection{Linear Solvers}

Dave May presented his work on robust solvers for the variable viscosity Stokes equation, which is summarized
in~\citet{May_Moresi_08}. He showed a series of benchmarks, culminating in a challenging problem with many small sinking
blocks. He showed that a stable element is necessary for the Schur complement to have a condition number independent of
the system size, meaning that the number of linear solver iterations can remain constant with increasing system
size. Details of stable element types, both macro elements and the stabilized element mentioned by Shijie, are given
in~\citet{ElmanSilvesterWathen05}. It also contains a large amount of information about Krylov methods and block
preconditioners for Stokes. The spectral equivalence between the pressure Schur complement and the scaled mass matrix is
proved in~\citet{GrinevichOlshanskii09}. Moreover, using a scalar multigrid solve on each component of the Stokes system,
while fine for the iso-viscous case, show notable growth in the number of iterations for these complex
variable-viscosity test cases. This kind of component-split preconditioning is used in both
Rhea~\citep{BursteddeGhattasStadlerTuWilcox09} and Aspect~\citep{KronbichlerHeisterBangerth12}, which therefore do not
solve the variable viscosity problem scalably.

Rajesh Kommu presented his work on introducing PETSc into CitcomS. He put a wrapper around the vector and matrix data structures
with PETSc Vec and Mat objects, which then allowed the PETSc linear solvers to be used. He has replicated the existing
Citcom linear solver using PETSc classes. Since all PETSc configuration is done dynamically, at runtime, solver
optimization should now be possible without much more effort. However, improvements are limited by the current
discretization scheme, and scalability is limited by the current parallel communication scheme. We discussed the changes
necessary in order to implement these scalable Stokes solvers in Citcom. It would involve a substantial rewrite in that
all the element routines would have to be changed, which would mean also changing all the parallel code. Thus, the end
product would be largely a completely new code. A possible alternative would be addition to the code used by Dave May to
accommodate Citcom input and produce Citcom output.

\subsection{{\large Energy Equation and Composition}}
The challenges for the energy solver in the mantle convection codes, CitcomCU, CitcomS and Aspect were discussed. In particular, the problems for temperature dependent viscosity with oscillations and overshoots. There was a discussion of monotone integrators and the problems with the current discretization for the diffusion equation. Gerry Puckett and student  Ted Studley presented work on the energy equation in Aspect. The possibility of working on a general solution for the problems with the energy solver in both CitcomCU/S and Apsect was recommended, as there are commonalities for the implementation in both codes.

It was agreed that it was important to formulate a discontinuous space in which finite volume or DG methods could be used.
Gerry and Ted were interested in pursuing the use of monotone FV methods defined on embedded subgrids.
Recent methods for positivity- and maximum principle-preserving limiters for DG and (W)ENO methods are also available~\citep{zhang2011maximum}.
A modest amount of refinement (internal to Stokes elements) appears to be computationally affordable and may be important to resolve fine-scale features over the domain, though a transport method that is nominally higher than second order is also likely to be important.
In case of a Cartesian grid, (W)ENO methods with limiters are straightforward to implement and the action of the limiters is more localized than with high-order DG.
With all these methods, it is important to have a velocity field that is discrete divergence-free, otherwise overshoots can arise even for the exact solution of the transport equation.
This can be achieved by using a discontinuous pressure space in the Stokes solve.

As an advection-dominated process (high cell P\'eclet number), the energy equation is very similar to methods for tracking composition, for which particles are popular.
We discussed whether field-based methods could be used for composition, as an alternative to the challenges of using particles.
Moment of Fluid methods~\citep{schofield2010multimaterial} have been enjoying popularity lately, though they are complicated to implement and the computational cost depends on the factorial of the number of materials in a given cell.
Several participants stated that most science could be accomplished with two or perhaps three materials, although others wanted at least ten.
Since it is unlikely that many materials would be present in the same cell, moment of fluid methods appear to be viable, though nobody has leapt to the task of implementing.
The alternative of simply using a monotone transport scheme (a strict maximum principle is crucial) was discussed, though it would require a filter to ensure that the fractions of all materials sum to 1 and would require increased subgrid resolution to control numerical diffusion.

\subsection{{\large Benchmarking and Uncertainty Quantification}}
As several options for implementing solver flexibility into the CitcomCU and CitcomS codes were presented, each varying in the degree of modifications of the existing source code, the topic of benchmarks was discussed. The importance of adequately benchmarking the individual codes as changes occur was emphasized as well as options for benchmarks across codes to compare code performance and usability. In particular, two kinds of benchmarks were highlighted: (a) benchmarks for accuracy in geophysical problems and (b) benchmarks for scaling. Shijie Zhong gave a presentation summarizing the benchmarking results for CitcomCU and CitcomS. Specifically, a series of benchmarks for CitcomCU are given in \citet{zhong2005dynamics,Zhong_2006}, and \citet{Zhong_et_al_00,Zhong_etal_08} contain benchmarks for CitcomS, including scaling information for models run on up to 3072 cores run on Ranger. In addition, a benchmark comparison for six mantle convection codes is given in \citet{King_et_al_2010} and subduction benchmarks are given in \cite{van2008community}. Pierre Arrial presented recent work on benchmarking a new mantle convection solver in collaboration with Louise Kellogg and the group at NCAR. In addition, Matthew Knepley suggested the exact solutions from Mirko Velic at Monash University.  \\
\\
With models increasing in complexity and grid size, the group discussed obtaining scaling information for up to 10K cores or larger 50K or 100K(?).  It was recommended to start with the benchmarks from \citet{Zhong_2006} and \citet{Zhong_etal_08}. In addition, it was recommended to generate a new benchmark for a 3D subduction model with large viscosity variations, although this would be difficult to test for accuracy without an analytic solution, but would be useful for comparison of code performance.  The general agreement was to start with about 5 benchmark cases, consisting of a few stokes solver cases for convection and a couple temperature dependent viscosity cases for testing the energy equation. \\
\\
Jed Brown presented the advantages and several approaches to better constraining the uncertainty of the convection solutions, quantification of the fit between model predictions and observational constraints, and quantifying the information value of different types of observations. This was agreed as an area of important research in the convection community, especially with the wealth of observational data now being acquired. It was emphasized to keep code modifications open, where possible, to further development in order to incorporate uncertainty quantification algorithms.
The methods used by the seismic tomography community are seen as being relatively mature and worth emulating where possible, though this is challenging since the models are nonlinear and data sources relevant to mantle convection and subduction are more diverse and tend to have poorly-understood and/or poorly-documented errors and correlations.

\section{\large Summary}
It was recommended to pursue the two levels of code modifications to improve the Stokes solvers in CitcomCU and CitcomS. Rajesh Kommu and Eric Heien will continue the implementation of solver functionality without changing the CitcomCU and CitcomS internal data structures. Matt Knepley and Margarete Jadamec will explore the degree of code modifications required in changing the internal data structures and testing its implementation. Shijie Zhong will consult on this effort. Contributions from other members of the mantle convection group/community are welcome.  Advantages of not changing the CitcomCU/S data structures included that users want to default to the old code, may not want to read the new code, and do not want to change the build system. Advantages of the deeper level code changes are the flexibility of interchanging solvers because, it was discussed that the experience with the codes thus far has shown that different solvers are optimal for different kinds of scientific problems. Thus, the less intrusive changes are limited in that sense. However, with the changes required to convert the data structures in CitcomCU/CitcomS to take advantage of the PETSc solver options, it may be worth exploring using Dave May's new code for very complex rheological problems. There is no answer for which is best. \\
\\
It was recommended that the Ted Studley and Gerry Puckett pursue the work on the energy equation in Aspect with cognizance of how to extend the modifications to the energy solver implementation in CitcomCU and CitcomS. This work will be done as a part of CIG in collaboration with Wolfgang Bangerth. Jed Brown and Matt Knepley will be available to consult on the solver options.\\
\\
It was recommended that the benchmarking cases ($\sim$ 5) be identified by the 2013 Fall AGU meeting or in the early spring of 2014.\\
\\
It was recommended to convene online meetings monthly or every fortnight during the fall of 2013 and assess/discuss progress on the code implementations. It was also recommend to meet in person at the CIG Business meeting at the AGU Fall 2013 meeting in San Francisco.\\
\\
A temporary code repository was set up on Bitbucket that will be migrated to the CIG hosted repository. In addition, it was recommended to move the main CIG CitcomCU and CitcomS repositories to Git due to the improved functionality of Git over the older SVN method for version control.\\\\


%%%
%%%
%%%
%\newpage
\section{{\large Workshop Information: Participant List and Workshop Agenda}}\label{sec:info}
{\bf Attendees}
\begin{itemize}
\item Katrina Arrendondo (UC Davis)
\item Pierre-Andre Arrial (CIG)
\item Jed Brown (ANL/PETSc)
\item Adam Holt (USC)
\item Eric Heien (CIG)
\item Lorraine Howang (CIG)
\item Margarete Jadamec (Brown Univ.)
\item Louise Kellogg (UC Davis)
\item Scott King (Virginia Tech., via email)
\item Rajesh Kommu (CIG)
\item Matt Knepley (Univ. Chicago, CIG, PETSc)
\item Dave May (ETH)
\item Gerry Puckett (UC Davis)
\item Alex Rice (Univ. Hawaii)
\item Ted Studley (UC Davis)
\item Matt Weller (Rice Univ.)
\item Shijie Zhong (CU Boulder)
\end{itemize}
